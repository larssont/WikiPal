# WikiPal
A discord bot for fetching information from wikipedia using the DiscordGo package.

### Prerequisites

In order to run this bot on your own machine, you need to install Go.

Follow the instructions here for your system: [golang.org/doc/install](https://golang.org/doc/install)

### Installing

`go get github.com/larssont/WikiPal`

`go install github.com/larssont/WikiPal`


## Built With

* [MediaWiki action API](https://www.mediawiki.org/wiki/API:Main_page) - Used to get data from wikipedia
* [DiscordGo](https://github.com/bwmarrin/discordgo) - Go package for discord chat client API

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
